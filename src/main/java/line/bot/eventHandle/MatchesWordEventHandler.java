//package line.bot.eventHandle;
//
//import line.bot.entity.IssueMessageWord;
//import org.springframework.data.rest.core.annotation.*;
//
//import java.util.logging.Logger;
//
//@RepositoryEventHandler(IssueMessageWord.class)
//public class MatchesWordEventHandler {
//
//    Logger logger = Logger.getLogger("Class MatchesWordEventHandler");
//
//    @HandleBeforeCreate
//    public void handleBookBeforeCreate(IssueMessageWord book){
//        logger.info("Inside IssueMessageWord Before Create....");
//
//    }
//
//    @HandleAfterCreate
//    public void handleBookAfterCreate(IssueMessageWord book){
//        logger.info("Inside IssueMessageWord After Create....");
//
//    }
//
//
//
//    @HandleAfterSave
//    public void handleBookAfterSave(IssueMessageWord book){
//        logger.info("IssueMessageWord Save After Create ....");
//
//    }
//
//    @HandleBeforeSave
//    public void handleBookBeforeSave(IssueMessageWord book){
//        logger.info("IssueMessageWord SaveBefore Create....");
//    }
//}
