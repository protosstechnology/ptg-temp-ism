package line.bot.repository;

import line.bot.entity.Priority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PriorityRepository extends JpaSpecificationExecutor<Priority>,
        JpaRepository<Priority, Long>,
        PagingAndSortingRepository<Priority, Long> {


}
