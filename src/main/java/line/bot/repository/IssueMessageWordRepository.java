package line.bot.repository;

import line.bot.entity.IssueMessageWord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IssueMessageWordRepository extends JpaSpecificationExecutor<IssueMessageWord>,
        JpaRepository<IssueMessageWord, Long>,
        PagingAndSortingRepository<IssueMessageWord, Long> {


}
