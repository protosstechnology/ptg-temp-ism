package line.bot.repository;

import line.bot.entity.LineLogMessage;
import line.bot.entity.MasterData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LineLogMessageRepository extends JpaSpecificationExecutor<LineLogMessage>,
        JpaRepository<LineLogMessage, Long>,
        PagingAndSortingRepository<LineLogMessage, Long> {


}
