package line.bot.service;

import line.bot.entity.LineLogMessage;
import line.bot.entity.MasterDataDetail;
import line.bot.repository.LineLogMessageRepository;
import line.bot.repository.MasterDataDetailRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class LineLogMessageServiceImp implements LineLogMessageService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LineLogMessageRepository lineLogMessageRepository;


    @Transactional
    @Override
    public Long insertLineLogMessage(LineLogMessage obj) {
        try{
            lineLogMessageRepository.saveAndFlush(obj);
            return obj.getId();
        }catch (Exception e){
                LOGGER.error(" ===- Exception[insertLineLogMessage] Msg : [{}]",e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
        }


    }
}
