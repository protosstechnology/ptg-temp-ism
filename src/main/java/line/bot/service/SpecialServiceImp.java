package line.bot.service;

import line.bot.entity.IssueMessageWord;
import line.bot.entity.LineLogMessage;
import line.bot.entity.Priority;
import line.bot.repository.IssueMessageWordRepository;
import line.bot.repository.LineLogMessageRepository;
import line.bot.repository.PriorityRepository;
import line.bot.util.ConstantVariable;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SpecialServiceImp implements SpecialService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    protected RestTemplate restTemplate;

    @Autowired
    PriorityRepository priorityRepository;

    @Autowired
    IssueMessageWordRepository issueMessageWordRepository;

    @Override
    public ResponseEntity<String> actionSpecial1() {
        /* waiting for implement */
        return null;
    }

    @Override
    public ResponseEntity<String> actionSpecial2() {
         /* waiting for implement */
        return null;
    }

    @Override
    public List<Map<String, Object>> generateKeyword() {
        List<Map<String,Object>> listResult = new ArrayList<>();
        LOGGER.info(" ===- IN generateKeyword -===");

        List<IssueMessageWord> matchesWordList = issueMessageWordRepository.findAll();
        LOGGER.info(" ===-  generateKeyword size list [{}]-===",matchesWordList.size());
        Map<String,Object> mapData = null;
        for(IssueMessageWord data : matchesWordList){
            mapData = new HashMap<>();
            mapData.put("inputText",data.getInputText());
            mapData.put("outputText",data.getOutputText());
            listResult.add(mapData);

        }

        LOGGER.info(" ===- OUT generateKeyword -===");
        return listResult;
    }

    @Override
    public int findHighPriorityMessage(String str) {
        LOGGER.info(" ===- IN findHighPriority -===");
        int result = 99;
        List<Priority> priorityList = priorityRepository.findAll();

        for(Priority data : priorityList){
            if(str.indexOf(data.getHasText()) != -1 && "Y".equals(data.getFlagActive())){
                result =  (data.getValue() < result ? data.getValue() : result);

            }

        }
        LOGGER.info(" ===- findHighPriority priority[{}] -===",result);

        LOGGER.info(" ===- OUT findHighPriority -===");

        return result;    }

    @Override
    public Map findWordMatches(String str, List<Map<String,Object>> list) {
        Map<String,Object> mapResult = new HashMap();
        mapResult.put("hasText",false);
        for(Map<String,Object> mapData : list){
            String text = String.valueOf(mapData.get("inputText"));
            int hasText = str.indexOf(text);
            if(hasText != -1){
                mapResult.put("hasText",true);
                mapResult.put("isText",mapData.get("outputText"));
                break;
            }
        }
        return mapResult;
    }
}
