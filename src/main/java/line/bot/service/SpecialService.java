package line.bot.service;

import line.bot.entity.LineLogMessage;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface SpecialService {

    public ResponseEntity<String> actionSpecial1();
    public ResponseEntity<String> actionSpecial2();
    public List<Map<String,Object>> generateKeyword();
    public int findHighPriorityMessage(String str);
    public Map findWordMatches(String text, List<Map<String,Object>> list);


}
