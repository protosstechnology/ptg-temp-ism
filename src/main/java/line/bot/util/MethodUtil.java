package line.bot.util;

import line.bot.entity.IssueMessageWord;
import line.bot.entity.Priority;
import line.bot.repository.IssueMessageWordRepository;
import line.bot.repository.PriorityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ratthanan-w on 8/5/2562.
 */
@Component
public class MethodUtil {
    private static Logger LOGGER = LoggerFactory.getLogger(MethodUtil.class);

    @Autowired
    private static IssueMessageWordRepository matchesWordRepositoryUtil;
    @Autowired
    private static PriorityRepository priorityRepositoryUtil;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);


    public static Timestamp getCurrentDate() {
        Timestamp today = null;
        try {
            Date nowDate = Calendar.getInstance().getTime();
            today = new java.sql.Timestamp(nowDate.getTime());
        } catch (Exception e) {
            LOGGER.error("error msg : {} ", e);
            throw new RuntimeException(e);
        }
        return today;
    }



    public static void generateKeyword() {
        ConstantVariable.LIST_KEYWORD = null;
        ConstantVariable.LIST_KEYWORD = new ArrayList<>();
        List<IssueMessageWord> matchesWordList = matchesWordRepositoryUtil.findAll();
        Map<String,Object> mapData = null;
        for(IssueMessageWord data : matchesWordList){
            mapData = new HashMap<>();
            mapData.put("inputText",data.getInputText());
            mapData.put("outputText",data.getOutputText());
            ConstantVariable.LIST_KEYWORD.add(mapData);

        }

    }


    public static Map findWordMatches(String str){
        Map<String,Object> mapResult = new HashMap();
        mapResult.put("hasText",false);
        for(Map<String,Object> mapData : ConstantVariable.LIST_KEYWORD){
            String text = String.valueOf(mapData.get("inputText"));
            int hasText = str.indexOf(text);
            if(hasText != -1){
                mapResult.put("hasText",true);
                mapResult.put("isText",mapData.get("outputText"));
                break;
            }
        }
        return mapResult;
    }



}
