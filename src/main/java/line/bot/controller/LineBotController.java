package line.bot.controller;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.*;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import line.bot.entity.IssueMessageWord;
import line.bot.entity.LineLogMessage;
import line.bot.entity.Priority;
import line.bot.repository.IssueMessageWordRepository;
import line.bot.repository.PriorityRepository;
import line.bot.service.AppLineBotDataService;
import line.bot.service.LineLogMessageService;
import line.bot.service.SpecialService;
import line.bot.util.ConstantVariable;
import line.bot.util.MethodUtil;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Slf4j
@LineMessageHandler
public class LineBotController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private AppLineBotDataService appLineBotDataService;

    @Autowired
    private LineLogMessageService lineLogMessageService;

    @Autowired
    private SpecialService specialService;

    @Autowired
    private static IssueMessageWordRepository issueMessageWordRepository;

    @Autowired
    private static PriorityRepository priorityRepositoryUtil;



    public static List<Map<String,Object>> LIST_KEYWORD = new ArrayList<>();


    @EventMapping
    public void handleTextMessage(MessageEvent<TextMessageContent> event) throws IOException {
        log.info(event.toString());
        handleTextContent(event.getReplyToken(), event, event.getMessage());
    }

//    private void handleTextContent(String replyToken, Event event, TextMessageContent content) throws IOException {
//        String text = content.getText();
//        String userId = event.getSource().getUserId();
//                if (appLineBotDataService.checkTextMatches(text)) {
//                    if (userId != null) {
//                        lineMessagingClient.getProfile(userId)
//                                .whenComplete((profile, throwable) -> {
//                                    if (throwable != null) {
//                                        this.replyText(replyToken, throwable.getMessage());
//                                        return;
//                                    }
//                                    this.reply(replyToken, Arrays.asList(
//                                            new TextMessage(appLineBotDataService.checkText(text))
//                                    ));
//                                });
//                    }else{
//                        this.replyText(replyToken, "User not found!!");
//                    }
//                }else{
//                    this.replyText(replyToken, "Case not found!!");
//                }
//    }
    private void replyText(@NonNull  String replyToken, @NonNull String message) {
        if(replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken is not empty");
        }

        if(message.length() > 1000) {
            message = message.substring(0, 1000 - 2) + "...";
        }
        this.reply(replyToken, new TextMessage(message));
    }

    private void reply(@NonNull String replyToken, @NonNull Message message) {
        reply(replyToken, Collections.singletonList(message));
    }

    private void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
        try {
            BotApiResponse response = lineMessagingClient.replyMessage(
                    new ReplyMessage(replyToken, messages)
            ).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

    }


        private void handleTextContent(String replyToken, Event event, TextMessageContent content) throws IOException {
            log.info(" ===- In[handleTextContent] Msg : [{}] -===",content.getText());

            List<Map<String,Object>> listResult = specialService.generateKeyword();
            try{
                String text = content.getText();
//                int priority = findHighPriority(text);
                int priority = specialService.findHighPriorityMessage(text);
                    if(priority == 0 ){
                        specialService.actionSpecial1();

                        this.reply(replyToken, Arrays.asList(
                                new TextMessage("Special Service 1 ")
                        ));
                    }else if (priority == 1){
                        specialService.actionSpecial1();
                        this.reply(replyToken, Arrays.asList(
                                new TextMessage("Special Service 2 ")
                        ));
                    }else if(priority == 3){

                        /* prepare object for insert*/
                        LineLogMessage obj = new LineLogMessage();

//                        obj.setId(Long.valueOf(String.valueOf((Math. random() * 9999 + 1))));
                        obj.setMsg(text);
                        obj.setPriority(priority);
//                        obj.setCreatedDate(new Timestamp(new Date().getTime()));
                        obj.setSender(content.getId());
                        /*insert Msg for DB*/
                        Long idIssue =   lineLogMessageService.insertLineLogMessage(obj);
                       /*replay with issue id after insert*/
                        if(idIssue != null){
                            log.info(" ===- [handleTextContent] Msg : [Save Issue Success] -===");
                        }else{
                            log.info(" ===- [handleTextContent] Msg : [Save Issue Fail] -===");
                        }



                    }else{
                        log.info(" ===- [handleTextContent] Msg : [Save Issue Success] -===");
                    }
                log.info(" ===- Out[handleTextContent] Msg : [{}] -===");
            }catch(Exception e){
                log.error(" ===- Exception[handleTextContent] Msg : [{}] -===",e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }

    }





}
