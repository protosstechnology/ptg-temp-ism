# ptg-temp-ism

# Build docker image
`$ mvn package dockerfile:build`

# Build docker image & Push to docker hub
`$ mvn clean package`
`$ mvn boost:docker-build`
`$ mvn boost:docker-push`

# Docker run
`$ docker run -p 9080:9080 ชื่อ docker`

